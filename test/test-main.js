
//card
var card = require('../test-card').card;
//deck
var deck = require('../test-deck').deck;
//utility code
var shuffle = require('../test-deck').shuffleArray;
//unity objects
var Vector3 = require('../test-unity').Vector3;
var Quaternion = require('../test-unity').Quaternion;
var Random = require('../test-unity').Random;
var Debug = require('../test-unity').Debug;
var Time = require('../test-unity').Time;
var GameObject = require('../test-unity').GameObject;
var Camera = require('../test-unity').Camera;
var Screen = require('../test-unity').Screen;
//touch and mouse input
var Input = require('../test-input').Input;


/** utility testing **/
var testShuffleArray = function (test){
    var different = false;
    var array = [1,2,3,4];

    array = shuffle(array);

    test.equal(array.length,4);

    test.notEqual(array.indexOf(1),-1);
    test.notEqual(array.indexOf(2),-1);
    test.notEqual(array.indexOf(3),-1);
    test.notEqual(array.indexOf(4),-1);

    if(array[0] != 1 || array[1] != 2 || array[2] != 3 || array[3] != 4){
        different = true;
    }

    test.ok(different,"The array is in the same order");

    test.expect(6);
    test.done();
};

/** unity object replacement verification **/
var testVector3 = function (test) {
    var vector = new Vector3(30,-10,22);

    test.equal(vector.x,30);
    test.equal(vector.y,-10);
    test.equal(vector.z,22);

    test.expect(3);
    test.done();
};

var testQuaternion = function (test) {
    var quat = new Quaternion(90,180,20);

    test.equal(quat.x,90);
    test.equal(quat.y,180);
    test.equal(quat.z,20);

    test.expect(3);
    test.done();
};

var testRandom = function (test) {
    var rand = Random();

    rand.seed = 3;

    test.equal(rand.seed,3);

    test.expect(1);
    test.done();
};

var testDebug = function (test){
    var debug = Debug();

    test.equal(debug.Log("hello"),"hello");

    test.expect(1);
    test.done();
};

var testTime = function (test){
    var time = Time();

    test.ok(time.start !== null, "Time requires start time on init");
    test.ok(time.time !== null, "Time requires time function");

    test.expect(2);
    test.done();
};

var testGameObject = function (test){
    var game = GameObject();

    var testCard = [{x:0},{x:1}];
    game.setDeck(testCard);

    test.equal(game.deck,testCard);
    test.equal(game.FindObjectsOfType("Card"),testCard);
    test.notEqual(game.FindObjectsOfType("NotCard"),testCard);
    test.notEqual(game.FindObjectsOfType(),testCard);

    test.expect(4);
    test.done();
};

var testCamera = function (test) {
    var cmra = Camera();
    var pos = cmra.position;
    var main = cmra.main;

    var curPos = new Vector3(0,0,10);

    test.equal(pos.x,0);
    test.equal(pos.y,10);
    test.equal(pos.z,0);

    test.expect(3);
    test.done();
};

var testScreen = function (test) {
    var scrn = Screen(100,200);

    test.ok(scrn.showCursor, "showCursor should start true");
    test.equal(scrn.width,100);
    test.equal(scrn.height,200);

    test.expect(3);
    test.done();
};

/** touch and mouse testing **/
var testMouseControl = function (test){
    var scrn = Screen(100,200);
    var testInput = Input(scrn);

    test.equal(testInput.mousePosition.x,scrn.width/2);
    test.equal(testInput.mousePosition.y,scrn.height/2);

    testInput.mousePosition = {x:3, y:10};

    test.equal(testInput.mousePosition.x,3);
    test.equal(testInput.mousePosition.y,10);

    test.expect(4);
    test.done();
};

var testTouchControl = function (test){
    var scrn = Screen(100,200);
    var testInput = Input(scrn);

    test.equal(testInput.GetTouch(0).position.x,scrn.width/2);
    test.equal(testInput.GetTouch(0).position.y,scrn.height/2);

    testInput.mousePosition = { x:5, y:-1 };

    test.equal(testInput.GetTouch(0).position.x,5);
    test.equal(testInput.GetTouch(0).position.y,-1);

    test.expect(4);
    test.done();
};

/** individual card testing **/
var testInitCard = function (test) {
    var testCard = card(0,0,0,2,4);

    test.equal(testCard.getSuit(),0);
    test.equal(testCard.getVal(),0);
    test.equal(testCard.GetInstanceID(),0);

    testCard.setSuit(3);
    testCard.setVal(12);

    test.equal(testCard.getSuit(),3);
    test.equal(testCard.getVal(),12);
    test.equal(testCard.GetInstanceID(),0);

    test.expect(6);
    test.done();
};

var testSetPosition = function (test) {
    var testCard = card(0,0,0,2,4);

    var pos = testCard.transform.position;

    test.equal(pos.x,0);
    test.equal(pos.y,0);
    test.equal(pos.z,0);

    testCard.setPos(45,33,-12);

    pos = testCard.transform.position;

    test.equal(pos.x,45);
    test.equal(pos.y,33);
    test.equal(pos.z,-12);

    test.expect(6);
    test.done();
};

var testSetRotation = function (test) {
    var testCard = card(0,0,0,2,4);

    var rotation = testCard.transform.localEulerAngles;

    test.equal(rotation.x,0);
    test.equal(rotation.y,0);
    test.equal(rotation.z,0);

    testCard.setRotation(180,90,180);

    rotation = testCard.transform.localEulerAngles;

    test.equal(rotation.x,180);
    test.equal(rotation.y,90);
    test.equal(rotation.z,180);

    test.expect(6);
    test.done();
};

var testCardMouse = function (test) {
    var scrn = Screen(100,200);
    var testInput = Input(scrn);

    var testCard = card(0,0,0,2,4);
    var moveDeltas = [{dx:0,dy:1},{dx:1,dy:1},{dx:-1,dy:0},{dx:1,dy:1},{dx:1,dy:0}];

    testCard.OnMouseDown(testInput);

    //move all moveDeltas
    for(var i=0; i < moveDeltas.length; i++){
        testInput.mousePosition.x += moveDeltas[i].dx;
        testInput.mousePosition.y += moveDeltas[i].dy;

        testCard.OnMouseDrag(testInput);

        test.equal(testCard.transform.position.x,testInput.mousePosition.x);
    }

    testCard.OnMouseUp(testInput);

    test.expect(moveDeltas.length);
    test.done();
};

var testCardTouch = function (test) {
    var scrn = Screen(100,200);
    var testInput = Input(scrn);

    var testCard = card(0,0,0,2,4);
    var moveDeltas = [{dx:0,dy:1},{dx:1,dy:1},{dx:-1,dy:0},{dx:1,dy:1},{dx:1,dy:0}];

    testCard.OnTouchBegan(testInput);

    //move all moveDeltas
    for(var i=0; i < moveDeltas.length; i++){
        testInput.mousePosition.x += moveDeltas[i].dx;
        testInput.mousePosition.y += moveDeltas[i].dy;

        testCard.OnTouchMoved(testInput);

        test.equal(testCard.transform.position.x,testInput.mousePosition.x);
    }

    testCard.OnTouchEnded(testInput);

    test.expect(moveDeltas.length);
    test.done();
};

var testUpdate = function(test) {
    var testCard = card();

    test.ok(testCard.update(),"card should start as client");

    testCard.isClient = false;

    test.ok(!testCard.update(),"card should now be server");

    test.expect(2);
    test.done();
};

/** deck and multicard testing **/
var testInitDeck = function (test){
    var testDeck = deck();
    var numCards = testDeck.initDeck(1);

    test.equal(numCards,52);

    test.expect(1);
    test.done();
};

var testGetCardsFromDeck = function (test) {
    var testDeck = deck();
    var numCards = testDeck.initDeck(1);
    var cards = testDeck.getCards();

    test.equal(cards.length,52);

    test.expect(1);
    test.done();
};

var testDeckContainsCards = function (test) {
    var testDeck = deck();
    var numCards = testDeck.initDeck(1);
    var cards = testDeck.getCards();

    for(var i=0; i<52; i++){
        test.equal(cards[i].GetInstanceID(),i);
    }

    test.expect(52);
    test.done();
};

/** utility testing **/
exports.testShuffleArray = testShuffleArray;

/** unity object replacement verification **/
exports.testVector3 = testVector3;
exports.testQuaternion = testQuaternion;
exports.testRandom = testRandom;
exports.testDebug = testDebug;
exports.testTime = testTime;
exports.testGameObject = testGameObject;
exports.testCamera = testCamera;
exports.testScreen = testScreen;

/** touch and mouse testing **/
exports.testMouseControl = testMouseControl;
exports.testTouchControl = testTouchControl;

/** individual card testing **/
exports.testInitCard = testInitCard;
exports.testSetPosition = testSetPosition;
exports.testSetRotation = testSetRotation;
exports.testCardMouse = testCardMouse;
exports.testCardTouch = testCardTouch;
exports.testUpdate = testUpdate;

/** deck and multicard testing **/
exports.testInitDeck = testInitDeck;
exports.testGetCardsFromDeck = testGetCardsFromDeck;
exports.testDeckContainsCards = testDeckContainsCards;
