var Camera = require('./test-unity').Camera();
var Random = require('./test-unity').Random();
var Debug = require('./test-unity').Debug();
var Time = require('./test-unity').Time();
var Vector3 = require('./test-unity').Vector3;
var Quaternion = require('./test-unity').Quaternion;
var Screen = require('./test-unity').Screen;
//var Input = require('./test-input').Input;

//create a js object for our card function testing, the card will have a size and position on a table
var card = function (suit,val,ID,width,height,GameObject){

    var self = {};

    self.spades = ["Spade - A","Spade - 2","Spade - 3","Spade - 4","Spade - 5","Spade - 6","Spade - 7","Spade - 8","Spade - 9","Spade - 10","Spade - J","Spade - Q","Spade - K",];
    self.hearts = ["Heart - A","Heart - 2","Heart - 3","Heart - 4","Heart - 5","Heart - 6","Heart - 7","Heart - 8","Heart - 9","Heart - 10","Heart - J","Heart - Q","Heart - K",];
    self.clubs = ["Club - A","Club - 2","Club - 3","Club - 4","Club - 5","Club - 6","Club - 7","Club - 8","Club - 9","Club - 10","Club - J","Club - Q","Club - K",];
    self.diamonds = ["Diamond - A","Diamond - 2","Diamond - 3","Diamond - 4","Diamond - 5","Diamond - 6","Diamond - 7","Diamond - 8","Diamond - 9","Diamond - 10","Diamond - J","Diamond - Q","Diamond - K",];

    //constructor variables
    self.suit = suit;
    self.val = val;
    self.ID = ID;
    self.width = width;
    self.height = height;

    //defaults
    self.lift = 0.5;
    self.isClient = true;
    self.cardPile = [];
    self.movePile = true;
    self.liftPile = true;
    self.lastClickTime = 0;
    self.catchTime = 0.25;
    self.startSingleMove = true;


    //non initialized
    self.screenPoint = null;
    self.offset = null;
    self.name = null;
    self.curScreenPoint = null;
    self.curPosition = null;

    self.bounds = {
        x: {
            min: -2,
            max: 2
        },
        y: {
            min: -2,
            max: 2
        },
        z: {
            min: -2,
            max: 2
        }
    };

    self.transform = {
        position: {
            x: 0,
            y: 0,
            z: 0
        },
        localEulerAngles: {
            x: 0,
            y: 0,
            z: 0
        },
        localScale: {
            x: 0,
            y: 0,
            z: 0
        },
        Find: function (text){
            return self;
        }
    };

    self.renderer = {
        material: {
            mainTexture: null
        }
    };

    self.Awake = function () {
        self.name = gameObject.GetInstanceID().ToString();
        var rotate = new Quaternion(0,180,0,0);
        self.transform.localEulerAngles.y += 180;
        //gameObject.renderer.material.mainTexture = setCardFace();
        self.transform.Find("Bottom").renderer.material.mainTexture = setCardFace();
        Random.seed = 3;
    };

    self.update = function () {
        if(self.isClient){
            Debug.Log("set pos update");
            //networking not set up in testing rig yet
            //networkView.RPC("SetPosition", RPCMode.All, transform.position);
            return true;
        }
        return false;
    };

    self.OnTouchStationary = function  (){
        self.createCardPile();
        self.movePile = true;
        self.liftPile = true;
        //moveBegin(Input.GetTouch(0).position);
    };

    self.GetInstanceID = function () {
        return self.ID;
    };

    self.setCardFace = function () {
        var tex;
        //var index: int = Random.Range(0,cards.length);
        var index = 0;
        var suit = self.suit;
        var val = self.val;

        switch(suit) {
            case 0:
                tex = self.spades[val];
                break;
            case 1:
                tex = self.hearts[val];
                break;
            case 2:
                tex = self.clubs[val];
                break;
            case 3:
                tex = self.diamonds[val];
                break;
            default:
                //default to ace of spades
                tex = self.spades[0];
        }
        return tex;
    };

    self.setSuit = function (suit) { self.suit = suit; };
    self.getSuit = function () { return self.suit; };
    self.setVal = function (val) { self.val = val; };
    self.getVal = function () { return self.val; };

    self.setPos = function (x,y,z) {
        self.transform.position = new Vector3(x,y,z);
    };

    self.setRotation = function (x,y,z){
        self.transform.localEulerAngles.x = x;
        self.transform.localEulerAngles.y = y;
        self.transform.localEulerAngles.z = z;
    };

    self.OnTouchBegan = function (Input) {
        self.moveBegin(Input.GetTouch(0).position);
    };

    self.OnTouchMoved = function (Input){
        self.moving(Input.GetTouch(0).position);
    };

    self.OnTouchEnded = function (Input){
        self.moveEnd(Input.GetTouch(0).position);
    };

    self.OnMouseDown = function (Input) {
        if(Time.time - self.lastClickTime < self.catchTime){
            //double click
            self.createCardPile();
            self.movePile = true;
            self.liftPile = true;
            self.moveBegin(Input.mousePosition);
            Debug.Log("doubleclick: " + (Time.time - lastClickTime));
        }
        else {
            self.moveBegin(Input.mousePosition);
        }
        self.lastClickTime = Time.time;
    };

    self.OnMouseDrag = function (Input) {
        self.moving(Input.mousePosition);
    };

    self.OnMouseUp = function (Input) {
        //Debug.Log("MouseUp");
        self.moveEnd(Input.mousePosition);
        self.clearCardPiles();
        self.movePile = false;
    };

    self.moveBegin = function (pos) {
        //networking not implemented yet
        //var viewID = Network.AllocateViewID();

        self.screenPoint = Camera.main.WorldToScreenPoint(self.transform.position);
        //need to do this separately for x,y,z
        var temp = Camera.main.ScreenToWorldPoint(new Vector3(pos.x, pos.y, self.screenPoint.z));

        self.offset = new Vector3(0,0,0);

        self.offset.x = self.transform.position.x - temp.x;
        self.offset.y = self.transform.position.y - temp.y;
        self.offset.z = self.transform.position.z - temp.z;

        //need to break this up
        self.transform.position.x += self.offset.x;
        self.transform.position.y += self.offset.y;
        self.transform.position.z += self.offset.z;


        //added this in testing
        self.curPosition = self.transform.position;

     //   transform.position.y += lift;
        self.transform.localScale.x += self.lift;
        self.transform.localScale.z += self.lift;
        Screen.showCursor = false;
        self.update();

    };

    self.moving = function (pos) {
        //update previous position so we can calculate delta
        self.prevPosition = self.curPosition;
        self.curScreenPoint = new Vector3(pos.x, pos.y, self.screenPoint.z);
        //need to break these up
        var temp = Camera.main.ScreenToWorldPoint(self.curScreenPoint);

        self.curPosition = new Vector3(0,0,0);

        self.curPosition.x = temp.x + self.offset.x;
        self.curPosition.y = temp.y + self.offset.y;
        self.curPosition.z = temp.z + self.offset.z;

        self.transform.position = self.curPosition;
        //delta is the vector we wish to apply to each card in the card pile
        //need to split this up

        self.delta = new Vector3(0,0,0);

        self.delta.x = self.curPosition.x - self.prevPosition.x;
        self.delta.y = self.curPosition.y - self.prevPosition.y;
        self.delta.z = self.curPosition.z - self.prevPosition.z;

        if (self.movePile) {
            for (var i = 0; i < self.cardPile.length; i++) {
                if (self.cardPile[i] != self) {
                    var card = self.cardPile[i];
                    //need to split this up
                    self.card.transform.position.x += self.delta.x;
                    self.card.transform.position.y += self.delta.y;
                    self.card.transform.position.z += self.delta.z;
                    if (self.liftPile) {
                        card.transform.localScale.x += lift;
                        card.transform.localScale.z += lift;
                    }
                }
            }
        }
        self.liftPile = false;
        self.update();
    };

    self.moveEnd = function (pos) {
        Screen.showCursor = true;
        self.transform.localScale.x -= self.lift;
        self.transform.localScale.z -= self.lift;
        for (var i = 0; i < self.cardPile.length; i++) {
            var card = self.cardPile[i];
            if (self.cardPile[i] != self) {
                card.transform.localScale.x -= lift;
                card.transform.localScale.z -= lift;
            }
        }

        //Andrew HEY!!

        //check if over player

        /*

        var temp: GameObject[];
        temp = GameObject.FindGameObjectsWithTag("Player");

        Debug.Log("Card z: " + gameObject.transform.position.z);
        Debug.Log("Plane z: " + temp[0].transform.position.z);


        if(gameObject.transform.position.z - 2 <= temp[0].transform.position.z){
            Debug.Log("CHECKING: " + temp[0].tag + " END");
            Debug.Log("Send to Player");
            temp[0].GetComponent(Player1).addCard(gameObject);

        }

        */

        self.update();
    };

    self.getPos = function () {
        return self.transform.position;
    };

    self.getChecked = function () {
        return self.isChecked;
    };

    self.setIsChecked = function (val) {
        if (val == true || val == false) {
            self.isChecked = val;
        }
    };

    self.createCardPile = function () {
        //iterate through game objects and find cards
        var game = GameObject.FindObjectsOfType("Card");
        for(var j=0; j < game.length; j++){
            var cur = game[j];  //attempts to cast all objects as cards
            if(cur != null){
                cur.setIsChecked(false);
            }
        }
        self.cardPile = self.fillCardPile();
        Debug.Log("Cardpile size: " + cardPile.length);
    };

    self.fillCardPile = function () {
        //Iterate through all cards in the game
        /*another option:
        / Have an array to keep track of the positions of every card.
        / Iterate through this array and determine if cards are overlapping.
        /
        */
        self.isChecked = true;
        var pile = [];
        var arrs = [];
        arrs.Push(self);
        var game = GameObject.FindObjectsOfType("Card");
        for(var j = 0; j < game.length; j++){
            var cur = game[j];
            //check if object is null, if not: is it unchecked
            if(cur != null && !cur.getChecked()){
                //the current object is a card and unchecked, check if it's overlapping
                if (self.checkOverlap(cur)) {
                    //it is overlapping:
                    //recursively call createCardPile() on current card
                    arrs = arrs.Concat(cur.fillCardPile());
                }
            }
        }
        for (var i = 0; i < arrs.length; i++) {
            var found = false;
            for (var k = 0; k < pile.length; k++) {
                if (arrs[i] == pile[k]) {
                    found = true;
                }
            }
            if (!found) {
                pile.Push(arrs[i]);
            }
        }
        return pile;
    };

    self.checkOverlap = function (card) {
        //Create var for position of card
        var curPos = card.getPos();

        //Setup edges vars
        //CardA: this card
        //CardB: card param
        //Debug.Log(transform.localScale);
        //Debug.Log(transform.localScale.x);
        //Debug.Log(transform.localScale.x/2);
        var leftEdgeA   = self.transform.position.x-(self.transform.localScale.x/2);
        var leftEdgeB   = curPos.x-(self.transform.localScale.x/2);
        var rightEdgeA  = self.transform.position.x+(self.transform.localScale.x/2);
        var rightEdgeB  = curPos.x+(self.transform.localScale.x/2);
        var topEdgeA    = self.transform.position.z+(self.transform.localScale.z/2);
        var topEdgeB    = curPos.z+(self.transform.localScale.z/2);
        var bottomEdgeA = self.transform.position.z-(self.transform.localScale.z/2);
        var bottomEdgeB = curPos.z-(self.transform.localScale.z/2);

        // check if found card overlaps with this card
        if ((rightEdgeB >= leftEdgeA && rightEdgeB <= rightEdgeA) || (leftEdgeB <= rightEdgeA && leftEdgeB >= leftEdgeA)) {
            //vertical edges are in overlapping region
            if ((topEdgeB >= bottomEdgeA && topEdgeB <= topEdgeA) || (bottomEdgeB <= topEdgeA && bottomEdgeB >= bottomEdgeA)) {
                //horizontal edges are in overlapping region
                //This is an overlapping case !!!!
                return true;
            }
        }
        //corner case where horizontal edges are touching
        else if (topEdgeA == bottomEdgeB || bottomEdgeA == topEdgeB) {
            return true;
        }
        //The cards can't possibly overlap
        return false;
    };


    self.emptyCardPile = function () {
        self.cardPile.Clear();
        //Debug.Log("cardPile now has: " + cardPile.length);
    };

    self.clearCardPiles = function () {
        var game = GameObject.FindObjectsOfType("Card");
        for(var j=0; j < game.length; j++){
            var cur = game[j];
            if (cur != null) {
                cur.emptyCardPile();
            }
        }
    };

    return self;
};

module.exports.card = card;

