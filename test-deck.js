var card = require('./test-card').card;
var GameObject = require('./test-unity').GameObject();

var DECK_52 = [
    [0,0],[0,1],[0,2],[0,3],[0,4],[0,5],[0,6],[0,7],[0,8],[0,9],[0,10],[0,11],[0,12],
    [1,0],[1,1],[1,2],[1,3],[1,4],[1,5],[1,6],[1,7],[1,8],[1,9],[1,10],[1,11],[1,12],
    [2,0],[2,1],[2,2],[2,3],[2,4],[2,5],[2,6],[2,7],[2,8],[2,9],[2,10],[2,11],[2,12],
    [3,0],[3,1],[3,2],[3,3],[3,4],[3,5],[3,6],[3,7],[3,8],[3,9],[3,10],[3,11],[3,12]
];

var shuffleArray = function (arr){
    var i = arr.length-1;

    while(i != 0){
        var j = Math.floor((Math.random()*i));
        var x = arr[i];
        arr[i] = arr[j];
        arr[j] = x;
        i -= 1;
    }
    return arr;
};

var deck = function () {
    var self = {};

    self.cards = [];

    self.initDeck = function (num_decks){
        var ID = 0;
        for(var i=0; i <num_decks; i++){
            for(var j=0; j<DECK_52.length; j++){
                var tempCard = card(DECK_52[j][0],DECK_52[j][1],ID,2,4,GameObject);
                self.cards.push(tempCard);
                ID++;
            }
        }
        GameObject.setDeck(self.cards);
        return self.cards.length;
    };

    self.getCards = function () {
        return self.cards;
    };

    self.shuffle = function (){

    };

    return self;
};

module.exports.deck = deck;
module.exports.shuffleArray = shuffleArray;