Setup:

    install node.js
        http://nodejs.org/

    install nodeunit globally
        sudo npm install nodeunit -g

    install istanbull globally
        sudo npm install istanbul -g

Run: (from the root project directory)

    unit tests:
        nodeunit test

    code coverage:
        istanbul cover nodeunit -- test/test-main.js



Other possible js/node testing/code coverage tools:

    testing:
        mocha
        karma
        nodeunit

    code coverage:
        istanbul
        blanket.js
        nodecover
