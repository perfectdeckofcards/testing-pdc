
function Vector3(x,y,z){
    this.x = x;
    this.y = y;
    this.z = z;
}

function Quaternion(x,y,z){
    this.x = x;
    this.y = y;
    this.z = z;
}

var Random = function () {
    self = {};

    self.seed = 0;

    return self;
};

var Debug = function (){
    self = {};

    self.Log = function (data){
        console.log(data);
        return data;
    };

    return self;
};

var Time = function (){
    self = {};
    var date = new Date();
    var start = date.getUTCSeconds();

    self.time = function (){
        return date.getUTCSeconds() - start;
    };

    return self;
};

var GameObject = function (){
    self = {};

    self.deck = null;

    self.setDeck = function(deck){
        self.deck = deck;
    };

    self.FindObjectsOfType = function (type) {
        if(type == "Card"){
            return self.deck;
        }
        else{
            return null;
        }
    };

    return self;
};

var Camera = function () {
     var self = {};

     self.position = {
        x: 0,
        y: 10,
        z: 0
     };

     self.main = {
        WorldToScreenPoint: function (pos) {
            //get the distance to the camera from the current point
            if(pos != null){
                //return new Vector3(self.position.x, self.position.y, self.position.z - pos.z);
                return pos;
            }
            //otherwise just return the camera z
            else{
                //return new Vector3(self.position.x, self.position.y, self.position.z);
                return pos;
            }
        },
        ScreenToWorldPoint: function (pos) {
            //get the distance to the camera from the current point
            if(pos != null){
                //return new Vector3(self.position.x, self.position.y, self.position.z - pos.z);
                return pos;
            }
            //otherwise just return the camera z
            else{
                //return new Vector3(self.position.x, self.position.y, self.position.z);
                return pos;
            }
        }
     };

    return self;
};

var Screen = function (width, height) {
    var self = {};
    self.width = width;
    self.height = height;
    self.showCursor =  true;
    return self;
};

module.exports.Vector3 = Vector3;
module.exports.Quaternion = Quaternion;
module.exports.Random = Random;
module.exports.Debug = Debug;
module.exports.Time = Time;
module.exports.GameObject = GameObject;
module.exports.Camera = Camera;
module.exports.Screen = Screen;