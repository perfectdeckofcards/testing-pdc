
//The bottom-left of the screen or window is at (0, 0). The top-right of the screen or window is at (Screen.width, Screen.height).
var Input = function (scrn) {
    self = {};

    var bounds = {
        x: {
            min: 0,
            max: scrn.width
        },
        y: {
            min: 0,
            max: scrn.height
        }
    };

    self.mousePosition = {
        x: scrn.width/2,
        y: scrn.height/2
    };

    self.GetTouch = function (index) {
        return {position:self.mousePosition};
    };

    return self;
};



module.exports.Input = Input;